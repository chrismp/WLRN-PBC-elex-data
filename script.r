# install.packages("dplyr")

## QUESTIONS TO ANSWER
# - Percent of registered voters who voted? (voter turnout)


require(dplyr)

path.2017 <- "2017"

misc.CSVs <- list.files(
  path = path.2017,
  pattern = "*.csv"
)

list.2017DFs <- list()
for(csv in misc.CSVs){
  misc.dfName <- gsub(
    pattern = ".csv",
    replacement = '',
    x = paste0("df.", path.2017, '.', csv)
  )
    
  assign(
    x = misc.dfName,
    value = read.csv(
      file = paste0(path.2017,'/',csv),
      header = TRUE
    )
  )
  list.2017DFs[[misc.dfName]] <- get(misc.dfName)
}

# Include only rows with total vote counts for each precinct
list.2017DFs <- lapply(
  X = list.2017DFs,
  FUN = filter,
  Ballot.type=="Total"
)

# Change NAs to 0
list.2017DFs <- lapply(
  X = list.2017DFs,
  FUN = function(x){
    replace(
      x = x,
      list = is.na(x),
      values = 0
    )
  }
)

# Add calculated columns
list.2017DFs <- lapply(
  X = list.2017DFs,
  FUN = transform,
  VoterTurnout = (as.numeric(Ballots.Cast) - as.numeric(Under.Votes) - as.numeric(Over.Votes)) / as.numeric(Registered)
)

list.2017DFs <- lapply(
  X = list.2017DFs,
  FUN = transform,
  RegisteredPercent = as.numeric(Registered) / sum(Registered)
)

list.2017DFs <- lapply(
  X = list.2017DFs,
  FUN = transform,
  BallotsCastPercent = as.numeric(Ballots.Cast / sum(Ballots.Cast))
)

list.2017DFs <- lapply(
   X = list.2017DFs,
   FUN = transform,
   RegisteredBallotsCastDiff = (RegisteredPercent - BallotsCastPercent) * 100
)

# Percent vote for the victors
list.2017DFs$`df.2017.Boca - Mayor`$HayniePercent <- list.2017DFs$`df.2017.Boca - Mayor`$Susan.Haynie / list.2017DFs$`df.2017.Boca - Mayor`$Ballots.Cast
list.2017DFs$`df.2017.Boca - Seat B`$ORourkePercent <- list.2017DFs$`df.2017.Boca - Seat B`$Andrea.Levine.O.Rourke / list.2017DFs$`df.2017.Boca - Seat B`$Ballots.Cast
list.2017DFs$`df.2017.Boynton - District 2`$McCrayPercent <- list.2017DFs$`df.2017.Boynton - District 2`$Mack.McCray / list.2017DFs$`df.2017.Boynton - District 2`$Ballots.Cast
list.2017DFs$`df.2017.Delray - District 2`$ChardPercent <- list.2017DFs$`df.2017.Delray - District 2`$Jim.Chard / list.2017DFs$`df.2017.Delray - District 2`$Ballots.Cast
list.2017DFs$`df.2017.Delray - District 4`$JohnsonPercent <- list.2017DFs$`df.2017.Delray - District 4`$Shirley.Johnson / list.2017DFs$`df.2017.Delray - District 4`$Ballots.Cast
list.2017DFs$`df.2017.Jupiter - District 1`$PosnerPercent <- list.2017DFs$`df.2017.Jupiter - District 1`$Wayne.R..Posner / list.2017DFs$`df.2017.Jupiter - District 1`$Ballots.Cast
list.2017DFs$`df.2017.Jupiter - District 2`$DelaneyPercent <- list.2017DFs$`df.2017.Jupiter - District 2`$Ron.Delaney / list.2017DFs$`df.2017.Jupiter - District 2`$Ballots.Cast
list.2017DFs$`df.2017.Lake Worth - District 2`$HardyPercent <- list.2017DFs$`df.2017.Lake Worth - District 2`$Omari.Hardy / list.2017DFs$`df.2017.Lake Worth - District 2`$Ballots.Cast
list.2017DFs$`df.2017.Lake Worth - District 4`$RobinsonPercent <- list.2017DFs$`df.2017.Lake Worth - District 4`$Herman.Robinson / list.2017DFs$`df.2017.Lake Worth - District 4`$Ballots.Cast
list.2017DFs$`df.2017.PBG - Group 1`$MarcianoPercent <- list.2017DFs$`df.2017.PBG - Group 1`$Mark.T..Marciano / list.2017DFs$`df.2017.PBG - Group 1`$Ballots.Cast
list.2017DFs$`df.2017.PBG - Group 3`$LanePercent <- list.2017DFs$`df.2017.PBG - Group 3`$Matthew.Jay.Lane / list.2017DFs$`df.2017.PBG - Group 3`$Ballots.Cast
list.2017DFs$`df.2017.PBG - Group 5`$LittPercent <- list.2017DFs$`df.2017.PBG - Group 5`$Rachelle.Litt / list.2017DFs$`df.2017.PBG - Group 5`$Ballots.Cast

# Merge demographic data with results data
list.2017DFs <- lapply(
  X = list.2017DFs,
  FUN = function(x){
    merge(
      x = x,
      y = read.csv(
        file = "Demographics/pctdem2.csv",
        header = TRUE
      ),
      by.x = "Precinct",
      by.y = "PREC"
    )
  }
)


# Write output to CSVs
lapply(
  X = 1:length(list.2017DFs),
  function(i){
    misc.filenamePart <- gsub(pattern = "df.2017.",
                              replacement = '',
                              x = names(list.2017DFs[i])
    )
    write.csv(
      x = list.2017DFs[[i]],
      file = paste0("Output/2017/",misc.filenamePart,".csv"),
      na = '0',
      row.names = FALSE
    )
  }
)


# Correlate demographics with voter turnout for each precinct
df.RacesCombined <- bind_rows(list.2017DFs)
cor(
  x = df.RacesCombined$AGE.17.29.PERCENT,
  y = df.RacesCombined$VoterTurnout,
  use = 'p',
  method = "pearson"
)

apply(
  X = df.RacesCombined,
  MARGIN = 2,
  FUN = cor,
  x = VoterTurnout,
  use = 'p',
  method = "pearson"
)

df.correlations <- round(
  x = cor(
    x = df.RacesCombined[sapply(df.RacesCombined,is.numeric)],
    use = 'p',
    method = "pearson"
  ),
  digits = 2
)

dummy <- NULL